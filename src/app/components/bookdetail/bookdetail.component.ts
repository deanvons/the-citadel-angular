import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-bookdetail',
  templateUrl: './bookdetail.component.html',
  styleUrls: ['./bookdetail.component.css'],
})
export class BookdetailComponent implements OnInit {
  public bookdetails: string = 'some booksdetails';

  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.bookdetails = `Some details for Book with Book Id of ${this.route.snapshot.paramMap.get(
      'id'
    )}`;
  }
}
