import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/Book';

@Component({
  selector: 'app-book-single',
  templateUrl: './book-single.component.html',
  styleUrls: ['./book-single.component.css'],
})
export class BookSingleComponent implements OnInit {
  @Input() Book: Book;

  constructor() { }

  ngOnInit(): void { }

  @Output() bookDetailRequested: EventEmitter<any> = new EventEmitter();

  onClick(Book) {
    //move to detail and pass id
    this.bookDetailRequested.emit(Book.BookId);
    console.log(Book.Title);
  }
}
