import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from '../../models/Book';
import { BooklistService } from '../../services/booklist.service';

@Component({
  selector: 'app-main-area',
  templateUrl: './main-area.component.html',
  styleUrls: ['./main-area.component.css'],
})
export class MainAreaComponent implements OnInit {
  Books: Book[];

  constructor(
    private bookListService: BooklistService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.Books = this.bookListService.getAllBooks();
  }

  showBookDetail(bookId) {
    this.router.navigateByUrl(`bookDetails/${bookId}`);
  }
}
