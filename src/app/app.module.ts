import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { MainAreaComponent } from './components/main-area/main-area.component';
import { BookSingleComponent } from './components/book-single/book-single.component';
import { HttpClientModule } from '@angular/common/http';
import { BookdetailComponent } from './components/bookdetail/bookdetail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    MainAreaComponent,
    BookSingleComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
