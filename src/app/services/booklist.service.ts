import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class BooklistService {
  constructor() {}

  getAllBooks() {
    return [
      {
        Title: 'Book 1',
        Genre: 'Some Genre',
        BookId: 1,
        CoverImg: 'ghththh.png',
        QtyOnHand: 3,
      },
      {
        Title: 'Book 2',
        Genre: 'Some Genre',
        BookId: 2,
        CoverImg: 'ghththh.png',
        QtyOnHand: 3,
      },
      {
        Title: 'Book 3',
        Genre: 'Some Genre',
        BookId: 3,
        CoverImg: 'ghththh.png',
        QtyOnHand: 3,
      },
    ];
  }

  getBookDetailById(bookId: number) {
    // return this.http
    //   .get(`https://localhost:44346/api/v1/Books/${bookId}`)
    //   .toPromise();
  }
}
