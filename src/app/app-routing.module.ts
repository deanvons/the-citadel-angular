import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookdetailComponent } from './components/bookdetail/bookdetail.component';

import { MainAreaComponent } from './components/main-area/main-area.component';

const routes: Routes = [
  {
    path: '',
    component: MainAreaComponent,
  },
  {
    path: 'bookDetails/:id',
    component: BookdetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
