export class Book {
  BookId: Number;
  Title: String;
  Genre: String;
  CoverImg: String;
  QtyOnHand: Number;
}
