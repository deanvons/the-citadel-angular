import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title: string = 'citadel-draft';

  constructor() {
    this.title = 'The Citadal';
    this.changeTitle('The Citadel');
  }

  changeTitle(newTitle): void {
    this.title = newTitle;
  }
}
